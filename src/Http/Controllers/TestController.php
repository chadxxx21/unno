<?php

namespace Unno\Test\Controllers;

use Unno\Test\Helper\TestHelper;
use Unno\Test\Http\Requests\TestRequest;

class TestController extends \App\Http\Controllers\Controller
{
    private $helper;

    public function __construct(TestHelper $helper)
    {
        $this->helper = $helper;
    }

    public function index(TestRequest $request)
    {
        return $this->helper->test($request->input('name'));
    }
}
