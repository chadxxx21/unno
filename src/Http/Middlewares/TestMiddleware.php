<?php

namespace Unno\Test\Http\Middleware;

use Closure;

class TestMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if ($request->has('name') && $request->input('name') === 'test'){
            abort(403, 'Access denied. Name is test.');
        }
        return $next($request);
    }
}
