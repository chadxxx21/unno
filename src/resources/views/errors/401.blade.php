@extends('errors.error')

@section('content')
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h3>Not authenticated!</h3>
                <h1><span>4</span><span>0</span><span>1</span></h1>
            </div>
            <h2>Please login to continue</h2>
        </div>
    </div>
@endsection

@section('script')

@endsection
