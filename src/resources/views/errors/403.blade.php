@extends('errors.error')

@section('content')
    <div id="notfound">
        <div class="notfound">
            <div class="notfound-404">
                <h3>Oops! Not Authorized!</h3>
                <h1><span>4</span><span>0</span><span>3</span></h1>
            </div>
            <h2>Access Denied</h2>
        </div>
    </div>
@endsection
