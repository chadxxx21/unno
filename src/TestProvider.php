<?php

namespace Unno\Test;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Unno\Test\Http\Middleware\TestMiddleware;

class TestProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @param Router $router
     */
    public function boot(Router $router)
    {
        $this->loadMigrationsFrom(__DIR__.'/Database/migrations');
        $this->loadRoutesFrom(__DIR__.'/routes/test.php');
        $this->loadViewsFrom(__DIR__.'/resources/views', 'Unno\\Test');

        $router->middlewareGroup('test', [TestMiddleware::class]);
    }
}
