<?php

use Illuminate\Support\Facades\Route;

Route::get('unno', function (){
    return 'This is a test';
});

Route::group(['middleware' => 'test'], function (){
    Route::get('unno-test', [\Unno\Test\Controllers\TestController::class, 'index']);
});

