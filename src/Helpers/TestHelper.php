<?php

namespace Unno\Test\Helper;

class TestHelper
{
    public function test($name)
    {
        if ($name === 'unno') {
            return 'Nayswun!';
        }

        return 'Ok!';
    }
}
