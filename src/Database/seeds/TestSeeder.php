<?php

class TestSeeder extends \Illuminate\Database\Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 10; $i++) {
            \Unno\Test\Models\Test::query()->create([
                'name' => 'name-' . $i,
                'age' => $i,
            ]);
        }
    }
}
