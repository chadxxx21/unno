<?php

namespace Unno\Test\Models;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $fillable = [
        'name',
        'age',
    ];

}
